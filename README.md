## Contents

1. **unet\_main\_cpu.exe** - tensorflow-cpu based executable for windows
2. **FileName2** - tensorflow-cpu based executable for ubuntu
3. **unet\_main\_gpu.exe** - tensorflow-gpu based executable for windows
4. **FileName4** - tensorflow-gpu based executable for ubuntu
5. **TrainCheckpointsNew** - directory containing the saved tensorflow model information
6. **Description.md** - contains a brief write-up on the approach

## Steps for downloading

The zipped archive can be downloaded by clicking on the green **clone or download** button in the top-right.

The repository can also be cloned using git by the command

    git clone https://github.com/pmcarpan/DIBCO19.git

## Instructions for running on windows

1. Open a command prompt window in the extracted/cloned directory.
2. **The TrainCheckpointsNew directory must be in the same directory as the executable being run.**
3. Execute the desired command from command prompt for running on cpu or gpu. 

Command for running on tf-cpu is

    unet_main_cpu.exe path/to/input/file.bmp path/to/output/output_file_name.bmp

Command for running on tf-gpu is

    unet_main_gpu.exe path/to/input/file.bmp path/to/output/output_file_name.bmp

## Instructions for running on ubuntu
